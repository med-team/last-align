last-align (1609-1) unstable; urgency=medium

  * New upstream version
  * Restrict to 64-bits architectures (routine-update)

 -- Charles Plessy <plessy@debian.org>  Wed, 19 Feb 2025 09:16:14 +0900

last-align (1608-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * Add missing trailing newline in debian/patches/series.

 -- Charles Plessy <plessy@debian.org>  Thu, 16 Jan 2025 13:59:43 +0900

last-align (1542-1) unstable; urgency=medium

  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Wed, 21 Feb 2024 08:06:34 +0900

last-align (1540-1) unstable; urgency=medium

  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Mon, 05 Feb 2024 09:27:51 +0900

last-align (1519-1) unstable; urgency=medium

  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Mon, 18 Dec 2023 04:22:22 +0000

last-align (1471-1) unstable; urgency=medium

  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Thu, 24 Aug 2023 01:52:30 +0000

last-align (1460-1) unstable; urgency=medium

  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Fri, 14 Jul 2023 09:37:02 +0900

last-align (1456-1) unstable; urgency=medium

  * New upstream version 1456

 -- Nilesh Patra <nilesh@debian.org>  Mon, 19 Jun 2023 19:17:17 +0530

last-align (1454-1) unstable; urgency=medium

  * New upstream version 1454. Re-diff patches
  * Update lintian overrides
  * Remove d/NEWS that has relevant news for
    39-1 release ~14 years back

 -- Nilesh Patra <nilesh@debian.org>  Mon, 12 Jun 2023 17:25:25 +0000

last-align (1447-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sun, 05 Feb 2023 21:03:18 +0100

last-align (1420-1) unstable; urgency=medium

  * Add myself back to uploaders
  * New upstream version 1420
  * Bump Standards-Version to 4.6.2 (no changes needed)

 -- Nilesh Patra <nilesh@debian.org>  Sat, 31 Dec 2022 17:18:26 +0530

last-align (1418-1) unstable; urgency=medium

  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Wed, 19 Oct 2022 09:29:30 +0900

last-align (1411-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 22 Sep 2022 09:28:20 +0200

last-align (1407-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 23 Aug 2022 16:53:32 +0200

last-align (1406-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 28 Jul 2022 16:49:02 +0200

last-align (1389-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 14 Jul 2022 16:39:54 +0200

last-align (1356-1) unstable; urgency=medium

  * New upstream version 1356

 -- Nilesh Patra <nilesh@debian.org>  Sun, 26 Jun 2022 00:46:20 +0530

last-align (1296-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1296
  * Bump Standards-Version to 4.6.1 (no changes needed)

 -- Nilesh Patra <nilesh@debian.org>  Sun, 29 May 2022 19:12:57 +0530

last-align (1282-1) unstable; urgency=medium

  * New upstream version 1282
  * Refresh patches

 -- Nilesh Patra <nilesh@debian.org>  Wed, 04 May 2022 15:09:54 +0530

last-align (1268-1) unstable; urgency=medium

  * New upstream version 1268

 -- Nilesh Patra <nilesh@debian.org>  Sat, 26 Feb 2022 23:52:43 +0530

last-align (1260-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 1260

 -- Nilesh Patra <nilesh@debian.org>  Wed, 16 Feb 2022 16:40:04 +0530

last-align (1257-2) unstable; urgency=medium

  * d/rules:
    + Pass CFLAGS and CXXFLAGS directly to dh_auto_build rather
      than exporting (Closes: #1003488)
    + export LC_ALL and LANG to use C.UTF-8 (Closes: #1003489)
    (Thanks to Vagrant Cascadian for the patches)

 -- Nilesh Patra <nilesh@debian.org>  Tue, 11 Jan 2022 13:54:19 +0530

last-align (1257-1) unstable; urgency=medium

  [ Vagrant Cascadian ]
  * Do not embed cpu-specific features in manpages
    Closes: #1003379

  [ Andreas Tille ]
  * Re-enable reprotest in Salca-CI to see whether the means above are
    working properly
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 10 Jan 2022 09:53:09 +0100

last-align (1256-1) unstable; urgency=medium

  * d/watch: Fix fetch URL - last-align has moved to gitlab
  * New upstream version 1256
  * d/p/helpMakefiles.patch: Refresh helpmakefile.patch
  * d/p/simde: Attempt adapting simde to new upstream
  * Drop Patches:
    - Drop d/p/fix-959866.patch: Since the problem has been internally
      fixed in simde
    - Drop d/p/2to3.patch: The new upstream is now compatible with both
      python2 and python3, hence these changes are un-needed.
      Added d/p/fix-shebang.patch to fix shebang in python script, as
      a replacement.
  * d/rules:
    - Attempt adapting to new upstream version
    - Add rest manpages
    - Install all lastdb5 based binaries
    - Install new fasta+fastq scripts
  * d/debian/last-align.doc-base.last-align-manual:
    The manual page has moved to last-cookbook.rst
  * d/tests/last-test: Add additional tests
  * d/control: Move python3 to Depends
    - There are several python3 scripts being vendored, it does make
      sense to promote python3 to depends
  * d/salsa-ci.yml: Disable blhc and reprotest
  * d/lintian-overrides: Add lintian override for maf-cut manpage

 -- Nilesh Patra <nilesh@debian.org>  Mon, 23 Aug 2021 00:56:44 +0530

last-align (1179-1) unstable; urgency=medium

  * New upstream version

 -- Nilesh Patra <npatra974@gmail.com>  Mon, 01 Feb 2021 16:02:34 +0530

last-align (1178-1) unstable; urgency=medium

  * New upstream version

 -- Nilesh Patra <npatra974@gmail.com>  Thu, 28 Jan 2021 17:19:32 +0530

last-align (1170-1) unstable; urgency=medium

  * New upstream version

 -- Nilesh Patra <npatra974@gmail.com>  Thu, 31 Dec 2020 17:04:25 +0530

last-align (1169-1) unstable; urgency=medium

  * New upstream version

 -- Nilesh Patra <npatra974@gmail.com>  Wed, 23 Dec 2020 12:13:08 +0530

last-align (1168-1) unstable; urgency=medium

  * New upstream version

 -- Nilesh Patra <npatra974@gmail.com>  Sat, 19 Dec 2020 16:19:45 +0530

last-align (1167-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)

 -- Nilesh Patra <npatra974@gmail.com>  Mon, 14 Dec 2020 23:14:25 +0530

last-align (1145-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Nilesh Patra <npatra974@gmail.com>  Wed, 11 Nov 2020 02:58:49 +0530

last-align (1133-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Nilesh Patra <npatra974@gmail.com>  Sat, 17 Oct 2020 13:20:28 +0530

last-align (1111-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 1111

 -- Nilesh Patra <npatra974@gmail.com>  Sat, 26 Sep 2020 02:11:54 +0530

last-align (1110-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Refreshed patches.

 -- Steffen Moeller <moeller@debian.org>  Sun, 20 Sep 2020 16:40:30 +0200

last-align (1080-1) unstable; urgency=medium

  * New upstream version
  * Refreshed patches

 -- Charles Plessy <plessy@debian.org>  Wed, 12 Aug 2020 11:12:45 +0000

last-align (1066-1) unstable; urgency=medium

  * Team upload.

  [ Michael R. Crusoe ]
  * i386: enable avx2, avx, and sse4.1 specific builds as well
  * Demote GNU parallel to a recommends; only used by parallel-fasta

  [ Steffen Moeller ]
  * New upstream version.
  * debhelper-compat 13 (routine-update)

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 17 May 2020 12:32:51 +0200

last-align (1061-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 10 May 2020 13:57:46 +0200

last-align (1060-5) unstable; urgency=medium

  * Team upload.
  * Fix i386 build.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Wed, 06 May 2020 21:01:18 +0200

last-align (1060-4) unstable; urgency=medium

  * Team upload.
  * Enable cross-building
  * Fix FTBFS on ppc64el, courtesy of frediz. (Closes: #959866)

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Wed, 06 May 2020 14:28:21 +0200

last-align (1060-3) unstable; urgency=medium

  * Team upload.
  * Use a newer libsimde-dev to fix a bug

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 19 Apr 2020 23:38:45 +0200

last-align (1060-2) unstable; urgency=medium

  * Team upload.
  * Switch to the libsimde-dev package from an internal copy
  * Added address of upstream's mailing list as Bug-Submit

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 19 Apr 2020 14:21:01 +0200

last-align (1060-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Cleaner clean

 -- Steffen Moeller <moeller@debian.org>  Tue, 24 Mar 2020 21:22:27 +0100

last-align (1047-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Fri, 24 Jan 2020 16:21:19 +0100

last-align (1045-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Updated simde patches

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Tue, 31 Dec 2019 08:42:29 +0100

last-align (1044-2) unstable; urgency=medium

  * Team upload.
  * Enable building on any architecture via simde. (Closes: #946850)
  * For any-amd64 and any-i386: compile multiple times with reducing levels of
    SIMD instruction support and ship a dispatching script to pick the best
    one for the user at run-time.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 22 Dec 2019 20:19:58 -0700

last-align (1044-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

  * TODO: hope for new upstream version that does not require
          SSE or AVX to compile.

 -- Steffen Moeller <moeller@debian.org>  Sat, 21 Dec 2019 00:28:04 +0100

last-align (1021-2) unstable; urgency=medium

  * Test-Depends: python3-pil
    Closes: #946209

 -- Andreas Tille <tille@debian.org>  Thu, 05 Dec 2019 15:01:59 +0100

last-align (1021-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Fiddling with CFLAGS/CXXFLAGS defaults in Makefile
  * fixed and minimized 2to3-patches - path for last-train
    so there is no FTBFS when generating man pages

  * TODO: Need to work on platform dependencies

 -- Steffen Moeller <moeller@debian.org>  Wed, 04 Dec 2019 13:35:17 +0100

last-align (984-2) unstable; urgency=medium

  * Use 2to3 to port to Python3
    Closes: #943148, #943446
  * Standards-Version: 4.4.1
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g

 -- Andreas Tille <tille@debian.org>  Fri, 22 Nov 2019 11:46:43 +0100

last-align (984-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Set fields Upstream-Contact in debian/copyright.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.

 -- Steffen Moeller <moeller@debian.org>  Wed, 04 Sep 2019 14:05:53 +0200

last-align (983-1) unstable; urgency=medium

  * Team upload.

  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Updated with no manual changes required by Andreas'
    routine-update script.

 -- Steffen Moeller <moeller@debian.org>  Sat, 27 Jul 2019 12:33:38 +0200

last-align (963-2) unstable; urgency=medium

  * Example files are not compressed any more - adapt autopkgtest
    Closes: #919556

 -- Andreas Tille <tille@debian.org>  Thu, 17 Jan 2019 11:21:30 +0100

last-align (963-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.3.0
  * debian/tests/control: Replace needs-recommends by explicitly
    adding recommended package python-pil to test depends

 -- Andreas Tille <tille@debian.org>  Fri, 11 Jan 2019 22:49:18 +0100

last-align (961-1) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.
  * Trim trailing whitespace.

  [ Andreas Tille ]
  * New upstream version
  * Remove trailing whitespace in debian/rules

 -- Andreas Tille <tille@debian.org>  Mon, 17 Dec 2018 11:13:03 +0100

last-align (956-1) unstable; urgency=medium

  * Team upload.

  * New upstream version.
  * Standards-Version: 4.2.1

 -- Steffen Moeller <moeller@debian.org>  Tue, 02 Oct 2018 15:07:34 +0200

last-align (938-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 23 May 2018 07:10:11 +0200

last-align (932-1) unstable; urgency=medium

  * New upstream version
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4

 -- Andreas Tille <tille@debian.org>  Thu, 26 Apr 2018 22:27:19 +0200

last-align (921-1) unstable; urgency=medium

  * New upstream version
  * make build compatible with -Wl, --as-needed (thanks for the patch to
    Steve Langasek <steve.langasek@canonical.com>)
    Closes: #889506
  * cme fix dpkg-control
  * debhelper 11
  * d/rules: do not parse changelog

 -- Andreas Tille <tille@debian.org>  Mon, 05 Feb 2018 10:39:42 +0100

last-align (885-1) unstable; urgency=medium

  * New upstream version 885
  * debian/rules: re-enable multi-threading.
  * debian/control: build-depend on zlib1g-dev.

 -- Charles Plessy <plessy@debian.org>  Sun, 15 Oct 2017 22:20:19 +0900

last-align (869-1) unstable; urgency=low

  * Team upload.

  [ Steffen Moeller ]
  * [debian/upstream/metadata] Added references to
    OMICtools, bio.tools, RRID

  [ Sascha Steinbiss ]
  * New upstream release.
  * Use python-pil instead of python-imaging as dependency.
    Closes: #866438
  * Add needs-recommends to d/t/control to fix autopkgtests.

 -- Sascha Steinbiss <satta@debian.org>  Fri, 11 Aug 2017 15:17:15 -0400

last-align (830-2) unstable; urgency=low

  * Team upload.

  [ Nadiya Sitdykova ]
  * Add autopkgtest test-suite

 -- Nadiya Sitdykova <rovenskasa@gmail.com>  Tue, 06 Jun 2017 01:56:56 -0400

last-align (830-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 23 Jan 2017 08:47:32 +0100

last-align (828-1) unstable; urgency=medium

  * New upstream version
  * Manpages for new binaries

 -- Andreas Tille <tille@debian.org>  Mon, 09 Jan 2017 14:16:28 +0100

last-align (809-1) unstable; urgency=medium

  * New upstream version
  * debhelper 10
  * d/watch: version=4

 -- Andreas Tille <tille@debian.org>  Wed, 30 Nov 2016 16:04:04 +0100

last-align (759-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 18 Oct 2016 13:43:13 +0200

last-align (755-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 10 Sep 2016 22:37:20 +0200

last-align (752-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 16 Aug 2016 14:35:15 +0200

last-align (746-1) unstable; urgency=medium

  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Fri, 15 Jul 2016 14:24:00 +0900

last-align (737-1) unstable; urgency=medium

  * New upstream version
  * cme fix dpkg-control
  * better hardening

 -- Andreas Tille <tille@debian.org>  Wed, 18 May 2016 18:08:29 +0200

last-align (731-1) unstable; urgency=medium

  * New upstream version
  * Fix debian/upstream/metadata
  * Use xz compression in watch opts
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Sat, 12 Mar 2016 08:01:37 +0100

last-align (712-1) unstable; urgency=medium

  [ Andreas Tille ]
  * New upstream version
  * Moved to Git
  * Add missing manpages
  * Depends: parallel (instead of Suggests)

  [ Julien Lamy ]
  * debian/rules:
    - Add missing build flags
    - Do not try to create manpage for removed tool

 -- Andreas Tille <tille@debian.org>  Thu, 31 Dec 2015 09:18:43 +0100

last-align (658-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 29 Oct 2015 16:23:59 +0100

last-align (621-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 24 Sep 2015 08:53:43 +0200

last-align (588-1) unstable; urgency=medium

  * New upstream version
  * Binaries lastex and last-pair-probs do not belong to the package any more
  * Removed paragraphs from copyright where files are not part of the
    source any more

 -- Andreas Tille <tille@debian.org>  Mon, 06 Jul 2015 18:00:21 +0200

last-align (490-1) unstable; urgency=medium

  * New upstream version
  * get-orig-source target now with xz compression
  * cme fix dpkg-control
  * Upstream dropped .py suffix - adpapt help2man calls in d/rules

 -- Andreas Tille <tille@debian.org>  Thu, 23 Oct 2014 22:30:48 +0200

last-align (475-1) unstable; urgency=medium

  * New upstream release.
  * debian/rules:
    - removed compression command, as it is now the default.
    - removed manual page of last-merge-batches.py.
  * debian/control: suggest GNU parallel.
  * debian/README.source: removed as conversion from zip is now automagic.

 -- Charles Plessy <plessy@debian.org>  Sat, 16 Aug 2014 15:20:37 +0900

last-align (418-1) unstable; urgency=medium

  * New upstream release
  * debian/upstream -> debian/upstream/metadata

 -- Andreas Tille <tille@debian.org>  Fri, 21 Mar 2014 14:57:56 +0100

last-align (393-1) unstable; urgency=medium

  * New upstream release
  * cme fix dpkg-control
  * debian/rules: Adapt help2man to the fact that two scripts were
    deleted from upstream source (no idea how to write manpages for the
    new parallel-* scripts)
  * Adapt doc-base control script (text file might be uncompressed)

 -- Andreas Tille <tille@debian.org>  Tue, 28 Jan 2014 17:57:09 +0100

last-align (359-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Removed Pre-Depends on dpkg as Ubuntu LTS is released.
    - Removed obsolete DM-Upload-Allowed field.
    - Use canonical VCS URLs.
    - Normalised with config-model-edit.
    - Removed version constraint on help2man, satisfied in oldstable.
    - Mark conformance with Policy version 3.9.4.
  * debian/watch: updated.
  * debian/rules:
    - Restore xz compression of the binary packages.
    - Produce a manual page for ‘last-split’ with help2man.

 -- Charles Plessy <plessy@debian.org>  Sun, 06 Oct 2013 12:16:12 +0900

last-align (199-1) unstable; urgency=low

  [ Charles Plessy ]
  * Summarise LAST license in debian/copyright header.

  [ Andreas Tille ]
  * New upstream version
    Closes: #667939
  * debian/rules:
     - switch from cdbs to dh
     - add help2man call for lastex
  * debian/install:
     - removed to use upstream install target (which also installs
       lastex)
  * debian/copyright: Make Source field more generic

 -- Andreas Tille <tille@debian.org>  Mon, 07 May 2012 13:28:25 +0200

last-align (198-1) unstable; urgency=low

  * New upstream version
  * debian/rules,debian/get-orig-source: Simplified downloading original
    source
  * debhelper 9 to enable hardening flags (control+compat, closes: #667939).

 -- Andreas Tille <tille@debian.org>  Mon, 02 Apr 2012 14:16:35 +0200

last-align (193-1) unstable; urgency=low

  * New upstream version
  * debian/upstream: Removed duplicate entries for DOI/PMID
  * debian/control: Standards-Version: 3.9.3 (no changes needed)

 -- Andreas Tille <tille@debian.org>  Thu, 08 Mar 2012 14:34:36 +0100

last-align (189-1) unstable; urgency=low

  * New upstream release.
  * Autogenerate a manual page for last-merge-batches (debian/rules).

 -- Charles Plessy <plessy@debian.org>  Sat, 22 Oct 2011 18:32:17 +0900

last-align (184-1ubuntu1) precise; urgency=low

  * Pre-Depends: dpkg (>= 1.15.6) for xz compression support.  Needed until
    after Ubuntu 12.04 LTS.

 -- Colin Watson <cjwatson@ubuntu.com>  Mon, 17 Oct 2011 21:40:04 +0100

last-align (184-1) unstable; urgency=low

  * New upstream release.
  * Corrected VCS URLs (debian/control).
  * Compress source and binary package with xz
    (debian/get-orig-source, debian/rules).

 -- Charles Plessy <plessy@debian.org>  Sat, 17 Sep 2011 20:31:16 +0900

last-align (178-1) unstable; urgency=low

  * New upstream release changing format of lastal output (r177).

 -- Charles Plessy <plessy@debian.org>  Thu, 30 Jun 2011 13:53:03 +0900

last-align (171-1) unstable; urgency=low

  * New upstream release accepting gcc-4.6.patch (LP: #770770).
    - Deleted debian/patches and its contents.
  * Install changelog through CDBS (debian/rules, debian/docs).
  * Proofread debian/copyright and refreshed DEP-5 format.

 -- Charles Plessy <plessy@debian.org>  Mon, 16 May 2011 22:01:53 +0900

last-align (162-1) unstable; urgency=low

  * New upstream version
  * Standards-Version: 3.9.2 (no changes needed)
  * debian/patches/gcc-4.6.patch: Fix gcc-4.6 build error
    Closes: #625084

 -- Andreas Tille <tille@debian.org>  Mon, 02 May 2011 15:29:14 +0200

last-align (159-1) unstable; urgency=low

  * New upstream version
  * Debhelper 8 (control+compat)
  * debian/last-align.doc-base.last-align-manual: last-manual
    seems to have dropped in favour of last-tutorial and other
    smaller files
  * debian/rules: Call help2man for new additional script

 -- Andreas Tille <tille@debian.org>  Fri, 11 Mar 2011 23:29:44 +0100

last-align (146-1) unstable; urgency=low

  * New upstream version
  * debian/rules:
    - Maf2html is not any more in the source so we need not to create
      the manpage
    - Use help2man to create maf-cull manpage
    - Remove src/lastex in clean target

 -- Andreas Tille <tille@debian.org>  Thu, 25 Nov 2010 19:46:56 +0100

last-align (139-1) unstable; urgency=low

  * New upstream version
  * Standards-Version: 3.9.1 (no changes needed)

 -- Andreas Tille <tille@debian.org>  Wed, 17 Nov 2010 11:22:38 +0100

last-align (128-1) unstable; urgency=low

  * New upstream version
  * Standards-Version: 3.9.0 (no changes needed)

 -- Andreas Tille <tille@debian.org>  Fri, 09 Jul 2010 12:12:04 +0200

last-align (119-1) unstable; urgency=low

  [ Charles Plessy ]
  * New upstream version.
  * debian/rules get-orig-source:
  * Pass --best and --no-name to gzip, for reproductible MD5 sums.
  * Add ‘.orig’ to the repacked directory, to conform with the Debian
    Developer’s Reference (§ 6.7.8.2.4).
  * Build-depend on help2man (>= 1.36.4+nmu1), since the option
    --version-string is not available earlier.
  * Added missing empty line in debian/NEWS.
  * Gathered information about upstream in debian/upstream-metadata.yaml.

  [ Andreas Tille ]
  * debian/last-align.doc-base.last-align-lastex:
    Upstream documentation evalue-tables.txt was replaced by lastex.txt
  * debain/rules:
    Added help2man call for maf-convert

 -- Andreas Tille <tille@debian.org>  Thu, 24 Jun 2010 11:41:46 +0200

last-align (102-1) unstable; urgency=low

  * New upstream version
  * debian/rules: scripts/maf2tab.py was removed -> do not try to
    create manpage for this script
  * debian/source/format: 3.0 (quilt)

 -- Andreas Tille <tille@debian.org>  Mon, 29 Mar 2010 12:29:16 +0200

last-align (99-1) unstable; urgency=low

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 11 Mar 2010 15:38:23 +0100

last-align (96-1) unstable; urgency=low

  * New upstream version
  * Standards-Version: 3.8.4 (no changes needed)

 -- Andreas Tille <tille@debian.org>  Thu, 18 Feb 2010 14:43:15 +0100

last-align (87-1) unstable; urgency=low

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 03 Nov 2009 10:47:02 +0100

last-align (70-1) unstable; urgency=low

  * New upstream version
  * Removed quilt dependency because upstream has applied patches
  * help2man for all scripts
    TODO: Write real manpages using the comments inside the scripts.
    Upstream confirmed that there are no immediate plans for further
    changes so writing static man pages seems to make sense

 -- Andreas Tille <tille@debian.org>  Mon, 05 Oct 2009 11:56:25 +0200

last-align (62-1) unstable; urgency=low

  * New upstream version
  * debian/control:
    - Added myself to upstream
    - Standards-Version: 3.8.3
    - Build-Depends: help2man, quilt, python-imaging
    - Recommends: python-imaging
  * debian/rules: Try to create manpages via help2man wherever possible
  * debian/patches: Make sure help output goes to stdout as GNU coding
    style suggests and is needed by help2man

 -- Andreas Tille <tille@debian.org>  Tue, 29 Sep 2009 13:38:57 +0200

last-align (58-1) unstable; urgency=low

  * New upstream release able to use sequence quality scores.

 -- Charles Plessy <plessy@debian.org>  Fri, 19 Jun 2009 20:47:02 +0900

last-align (52-1) unstable; urgency=low

  * New upstream releases (52 and 51).
    - Added maf2html.py: converts MAF-format alignments to coloured HTML format.
    - Made lastal gapped extension faster, by removing an assert (surprising).
    - Bugfix: made last-dotplot.py not crash on small sequences.

 -- Charles Plessy <plessy@debian.org>  Mon, 18 May 2009 23:52:24 +0900

last-align (50-1) unstable; urgency=low

  * New upstream release (minor changes).

 -- Charles Plessy <plessy@debian.org>  Tue, 21 Apr 2009 09:59:54 +0900

last-align (48-1) unstable; urgency=low

  * New upstream release.
    - maf-join.py: joins pairwise alignments into multiple alignments.
    - Changed lastal default gap existence cost for DNA to 7.
    - Changed lastal default gapped score threshold for DNA to 40.
    - maf-swap.py: Added -n option to move any sequence to the top.
    - Allow suffixes K(ibibyte), M(ebibyte), and G(ibibyte) for sizes.
    - Plus other bugfixes and improvements, please refer Upstream's changelog.
  * Incremented Standards-Version in debian/control to reflect conformance
    with Policy 3.8.1 (no changes needed).
  * Simplified machine-readable debian/copyright file.
  * last-align.doc-base.last-align-scripts: last-scripts.txt is now compressed.

 -- Charles Plessy <plessy@debian.org>  Wed, 08 Apr 2009 19:31:28 +0900

last-align (42-1) unstable; urgency=low

  * New upstream release:
    - Corrects an overflow error that made lastal discard some gapped
      alignments.
    - Allows lastal options to be set in the score-matrix file.
  * debian/copyright refreshed and points directly to GPLv3.
  * Corrected the description in debian/control to not use
    the first person.

 -- Charles Plessy <plessy@debian.org>  Tue, 10 Mar 2009 19:46:32 +0900

last-align (39-1) unstable; urgency=low

  * New upstream release.
    - Bugfix: for protein alignments, tyrosine was forbidden in initial
      matches.
    - *Old lastdb protein databases won't work properly: you need to re-run
      lastdb.*
  * Added debian/NEWS.Debian to indicate the backward incompatibility.
  * Debian/changelog: removed vanity entries and incremented copyright year.
  * Transferred informations about packaging practices and repository from
    README.source to README.Debian.

 -- Charles Plessy <plessy@debian.org>  Mon, 02 Feb 2009 12:42:26 +0900

last-align (37-1) unstable; urgency=low

  * New upstream release.
    - Updated the documentation, including a table of optimal spaced seeds.
    - Made lastdb bucket-making code slightly simpler & faster.
    - Enabled reading & writing of huge files (at least on one system with
      Linux-2.6.13/x86_64/gcc-4.0.2).
    - Made lastdb radix sort a bit faster, with the oracle array trick.
    - Allowed the mismatch score to be zero, due to a request.
    - Made gaped extension faster for short sequences (e.g. tag mapping).
    - Bugfix: the sequence names weren't printed correctly for
      match-counting output (-j 0).
    - Fixed compiler warnings with gcc 4.3.

 -- Charles Plessy <plessy@debian.org>  Sun, 28 Dec 2008 12:49:05 +0100

last-align (31-1) unstable; urgency=low

  * Initial release (Closes: #498562)

 -- Charles Plessy <plessy@debian.org>  Thu, 13 Nov 2008 15:44:31 +0900
